const express = require('express');
var router = express.Router();

const authService = require('../service/authService');


router.post('/sign-in',authService.authenticateUser);
router.post('/sign-up', authService.newUerSingUp);

module.exports = router;