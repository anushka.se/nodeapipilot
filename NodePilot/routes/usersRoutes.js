const express = require('express');
const router = express.Router();

const UserService = require('../service/userService');
const apiResponse = require('../helpers/apiResponse');
const UserController = require('../controllers/UserController');
const authService = require('../service/authService');

// router.get('/users',async (req, res)=>{
//     var userService = new UserService();
//     //res.json({ message: `TODO: ${userService.getAllUsersList()}` });
//     let data = userService.getAllUsersList();
//     return apiResponse.successResponseWithData(res, "Operation success", data);
// });

router.get('/users',authService.authorizeUser,UserController.usersList);

module.exports = router;