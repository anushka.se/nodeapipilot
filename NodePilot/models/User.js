const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  
  userId: {
    type: String,
    required: true,
  },
  userName: {
    type: String,
    required: true,
  },
  userEmail: {
    type: String,
    required: true,
  },
  passwordHash: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    required: true,
  },
})
module.exports = mongoose.model('User',userSchema)