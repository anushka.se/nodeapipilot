const mongoose = require('mongoose');
const configs = require('../config');

const initConnection = () => {

    mongoose.connect(configs.db_connection_string, { useNewUrlParser: true });
    var db = mongoose.connection;

    // Added check for DB connection
    if (!db)
        console.log("Error connecting db.")
    else
        console.log("db connected.")
}

module.exports = { initConnection };