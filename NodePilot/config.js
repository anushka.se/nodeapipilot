const dotenv = require('dotenv');
dotenv.config();
module.exports = {
  endpoint: process.env.API_URL,
  masterKey: process.env.API_KEY,
  port: process.env.PORT,
  jwt_secret: process.env.JWT_SECRET,
  pot: process.env.PORT,
  jwt_expires_in: process.env.JWT_EXPIRES_IN,
  db_connection_string: process.env.DB_CONNECTION_STR
};