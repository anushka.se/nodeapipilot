const jwt = require("jsonwebtoken");
const configs = require('../config');
const apiResponse = require('../helpers/apiResponse');
const validate = require('node-model-validation');
const bcrypt = require('bcrypt')

const UserModel = require('../models/User');

const authorizeUser =  (req, res, next)=> {

    const token = req.headers['x-access-token'];
    if (!token) {
        return res.status(403).send("A token is required for authorization");
    }
    try {
        req.user = jwt.verify(token, process.env.JWT_SECRET);
    } catch (err) {
        return apiResponse.unauthorizedResponse(res, 'Invalid Token');
    }
    return next();
};

const authenticateUser = async (req, res, next) =>{

    try {
        var refModel = {
            email:{
                type: String,
                isRequired: true
            },
            Password:{
                type: String,
                isRequired: true
            }
        };

        //validate request model
	    let validationResult = validate.validate(req.body, refModel);
        if(validationResult && validationResult.errors)
        {
            return apiResponse.validationErrorWithData(res,'Error',validationResult.errors, 'vlaidationError');
        }

        //TODO:check user existance

        //TODO:validate password


        //if all ok, create a new session/token
        var responseObject = generateAccessToken(45,'sdferexvrermnrx0df00jj');
        return apiResponse.successResponse(res,'Token Created',responseObject);

    }catch (err){
        console.log(err)
         next(err);
    }
};

const newUerSingUp = async (req, res, next) => {
    try{
        var newUerSingUpRefModel = {
            userName:{
                type: String,
                isRequired: true
            },
            userEmail:{
                type: String,
                isRequired: true
            },
            password:{
                type: String,
                isRequired: true
            }
        };

        //validate request model
	    let validationResult = validate.validate(req.body, newUerSingUpRefModel);
        if(validationResult && validationResult.errors)
        {
            return apiResponse.validationErrorWithData(res,'Error',validationResult.errors, 'vlaidationError');
        }

        let newUser = new UserModel(
            {
                userId:99,
                userEmail: req.body.userEmail,
                userName: req.body.userName,
                passwordHash: await generateHash(req.body.password, 10),
                createdDate: Date.now()
            });

        await newUser.save()
        .then(data => {
            return apiResponse.successResponseWithData(res,'user created.', data)
        })
        .catch(error => {
            return apiResponse.ErrorResponse(res,'Faied To save', error);
        });

    }catch (err){
        console.log(err)
        next(err);
    }
};

function generateHash(input,saltRounds){
    return new Promise((resolve,reject) =>{
        bcrypt.genSalt(saltRounds, function(err, salt) {  
            bcrypt.hash(input, salt, function(err, hash) {
              resolve(hash);
            });
          });
    });
}

//The sessionId should be generate using particular user data
//may be login time then after 2hrs it can be used to session timeout
const generateAccessToken = (id,sessionId) => {
    return jwt.sign(
      {
        id,
        sessionId
      },
      configs.jwt_secret,
      {
        expiresIn: configs.jwt_expires_in,
      },
    );
  };


module.exports = {authorizeUser, authenticateUser, newUerSingUp};