const UserService = require('../service/userService');
const apiResponse = require('../helpers/apiResponse');

/**
 * Users List.
 * @returns {Object}
 */
 exports.usersList = [
	 function (req, res) {
		try {
			var userService = new UserService();
            let data = userService.getAllUsersList();
            return apiResponse.successResponseWithData(res, "Operation success", data);
		} catch (err) {
			//throw error in json response with status 500. 
			return apiResponse.ErrorResponse(res, err);
		}
	}
];