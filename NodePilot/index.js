const express = require('express');
const xss = require('xss-clean');
const cors = require('cors');
const hpp = require('hpp');

const userRouter = require('./routes/usersRoutes');
const authRouter = require('./routes/authRoutes');
const configs = require('./config');
const db = require('./middlewears/dbContext');

const app = express();



// Allow Cross-Origin requests
app.use(cors());
// Data sanitization against XSS(clean user input from malicious HTML code)
app.use(xss()); 

// Prevent parameter pollution
app.use(hpp());

// Set security HTTP headers
// app.use(helmet());

// Limit request from the same API 
// const limiter = rateLimit({
//     max: 150,
//     windowMs: 60 * 60 * 1000,
//     message: 'Too Many Request from this IP, please try again in an hour'
// });
// app.use('/api', limiter);

// Data sanitization against Nosql query injection
// app.use(mongoSanitize());

//init DBcontext
db.initConnection();

app.disable('etag');//if remove this browser will get 304 status(meaning it will cache the page)

// Body parser, reading data from body into req.body
app.use(express.json({
    limit: '15kb'
}));



const port = configs.port || 3000;


app.use('/api/user',userRouter);
app.use('/api/auth',authRouter);


app.listen(port,()=> {
    console.log(`Server is up & running on port: ${port}`);
});